# CFRets

Extends the [Trulia RETS client](https://github.com/trulia/trulia-java-rets-client) to be accessed via Coldfusion. Really just a test repo for now.

## Install

The libraries are stored in Git submodules for rapid development. Clone them via

```
git clone --recursive git@bitbucket.org:mbornBCG/cfrets.git
```

Start the Lucee server via Commandbox:

```
box
server start
```

## Development

First initialize `javaloader` so it can create Java classes we need. Note use of jar with dependencies.

```js
loadPaths = [ExpandPath("lib/retsclient/target/rets-client-local-jar-with-dependencies.jar")];
request.javaloader = createObject("component", "lib.javaloader.javaloader.JavaLoader").init( loadPaths );
```

Next, load the classes you need for a RETS integration.

```js
HTTPClient = request.javaloader.create("org.realtors.rets.client.CommonsHttpClient");
RETSSession = request.javaloader.create("org.realtors.rets.client.RetsSession");
```

You can init these classes and follow the [Trulia README](https://github.com/trulia/trulia-java-rets-client/blob/master/README.md) from here.

```js
loginURL = "http://rets.example.com";

// not sure how to correctly specifiy the rets version
// RetsVersion retsVersion = RetsVersion.RETS_1_7_2;

// prepare for login!
RETSSession.init(loginURL,HTTPClient,retsVersion);

// try to authenticate with the RETS server
RETSSession.login(authUsername, authPassword);
```