component {
	
	this.name="cfrets";
	
	public void function onRequestStart() {
		// note we use the jar with dependencies to avoid machine-specific problems.
		LOCAL.loadPaths = [ExpandPath("lib/retsclient/target/rets-client-local-jar-with-dependencies.jar")];
		request.javaloader = createObject("component", "lib.javaloader.javaloader.JavaLoader").init( LOCAL.loadPaths );
	}
}