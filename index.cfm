<cfscript>
	// test
	writeOutput("Hello world!");
	
	// get credentials
	include "config.cfm";
	writeDump( config );
	
	// info
	WriteOutput('<h2>Javaloader paths</h2>');
	writeDump( request.javaloader.getClassLoadPaths() );
	
	// load the http client
	writeOutput('<h2>RETS http client</h2>');
	HTTPClient = request.javaloader.create("org.realtors.rets.client.CommonsHttpClient").init();
	RETSSession = request.javaloader.create("org.realtors.rets.client.RetsSession");
	retsVersion = request.javaloader.create("org.realtors.rets.client.RetsVersion");

	// set rets version
	retsVersion = retsVersion.init(1,7,2,0);
	
	// prepare for login!
	RETSSession.init(config.rets_uri,HTTPClient,retsVersion);
	RETSSession.setMethod("GET");
	
	// debugging
	writeDump("Rets Version: " & retsVersion.toString());
	writeDump("Logging in to " & RetsSession.getLoginUrl());
	
	
	// try to authenticate with the RETS server
	RETSSession.login(config.creds.username, config.creds.password);
	
	
	
	
	// NOW DO A SEARCH
	searchFor = "Property";
	searchClass = "ResidentialProperty";
	searchQuery = "";
	searchRequest = request.javaloader.create("org.realtors.rets.client.SearchRequest").init(searchFor,searchClass,searchQuery);
	
	// get small number of fields
	// @cite http://www.getoffutt.com/rvhs/srch.php
	//searchRequest.setFormatCompact();
	searchRequest.setSelect("listingID,listPrice,City,StateOrProvince,latitude,longitude,PublicRemarks");
	
	// only get X records
	searchRequest.setLimit(20);
	
	// WORKS?
	
	searchResponse = RETSSession.search(searchRequest);
	
	writeOutput("<h2>Search Results</h2>");
	writeDump(searchResponse.getRowCount() & " results");
	// Iterate over, print records
	for (row = 0; row < searchResponse.getRowCount(); row++){
		writeDump(searchResponse.getRow(row));
	}
</cfscript>